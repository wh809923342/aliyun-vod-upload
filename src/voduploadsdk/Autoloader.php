<?php

namespace Upload\Vod;

class Autoloader{

    public static function register($prepend = false)
    {
        spl_autoload_register(array(new self(), 'autoload'), true, $prepend);
        require_once  __DIR__ . DIRECTORY_SEPARATOR . 'aliyun-php-sdk-core' . DIRECTORY_SEPARATOR . 'Config.php';
        require_once  __DIR__ . DIRECTORY_SEPARATOR . 'aliyun-php-sdk-oss' .DIRECTORY_SEPARATOR . 'autoload.php';
    }

    public function autoload($class)
    {
        $path = str_replace('\\', DIRECTORY_SEPARATOR, $class);
        $file = __DIR__ . DIRECTORY_SEPARATOR . 'uploader'. DIRECTORY_SEPARATOR . $path . '.php';
        if (file_exists($file)) {
            require_once $file;
        }
    }
}

